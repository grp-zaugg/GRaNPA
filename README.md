### GRaNIE and GRaNPA: Inference and evaluation of enhancer-mediated gene regulatory networks

The companion packages *GRaNIE* (**G**ene **R**egul**a**tory **N**etwork **I**nference including **E**nhancers) and *GRaNPA* (**G**ene **R**egul**a**tory **N**etwork **P**erformace **A**nalysis) are currently under active development. If you have questions, please do not hesitate to contact us (see below).

### [Visit the GRaNPA Webpage](https://grp-zaugg.embl-community.io/GRaNPA/index.html)

For *GRaNIE*, please visit [this page](https://grp-zaugg.embl-community.io/GRaNIE).



### Summary

An important step in the regulatory network reconstruction is to evaluate their biological significance. Common approaches for assessing regulatory interactions include benchmarking them against networks from simulated data or against known biological networks. The drawback of simulated networks is that they are based on many assumptions of the structure of a “true” biological network. Benchmarking against true biological networks typically suffer from a strong literature bias, low complexity of these “true” circuits and a limited range of connections and cell types. Here we present *GRaNPA* to assesses the biological relevance any TF-Gene GRNs using a machine learning framework to predict cell-type specific differential expression. 
*GRaNPA* requires differential RNA expression data that is independent from the data used to generate the GRN. It then trains a random forest regression model to predict a differential expression value per gene, based on the TF-target gene connections from the GRN in a 10-fold cross validation setting (Out Of Bag and Bootstraping is also available). To assess the specificity of the network, it also trains a separate model based on randomized edges of similar structure and compares the obtained predictions (R2). A performance (R2) greater than 0.05 for the random networks indicates that even unspecific TF-gene connections can predict differential expression. To assess overfitting, GRaNPA trains the same random network on completely random differential expression data (uniform distribution between -10 to 10; see methods). A performance greater than 0.05 for a random network to predict random values indicates that the model is overfitting and can predict anything. Notably, *GRaNPA* can be applied to benchmark GRNs constructed using various methods.





### Get help and contact us

If you have questions or comments, feel free to contact us. We will be happy to answer any questions related to this project as well as questions related to the software implementation. For method-related questions, contact Judith B. Zaugg (judith.zaugg@embl.de). For technical questions, contact Aryan Kamal (aryan.kamal@embl.de).

If you have questions, doubts, ideas or problems, please use the [Gitlab Issue Tracker](https://git.embl.de/grp-zaugg/GRaNPA/issues). We will respond in a timely manner.

