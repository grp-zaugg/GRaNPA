Package: GRaNPA
Type: Package
Title: GRaNPA: Evaluation of TF-gene regulatory network using prediction of differential expression data
Version: 0.1.16
Authors@R: c(person("Aryan", "Kamal", email =
        "aryan.kamal@embl.de", role = c("cre","aut")),
        person("Judith", "Zaugg", email =
        "judith.zaugg@embl.de", role = "aut"))
Description: Among the biggest challenges in the post-GWAS (genome-wide association studies) era is the interpretation of disease-associated genetic variants in non-coding genomic regions. Enhancers have emerged as likely mediators of common genetic variants, likely through their regulatory effect on gene expression. Their activity is regulated by a combination of transcription factor activities, epigenetic changes and genetic variants. Several approaches exist to link enhancers to their target genes, and others that infer transcription factor-gene connections. However, we currently lack a framework to systematically integrate enhancers into gene-regulatory networks. Furthermore, we lack an unbiased way to assess whether inferred regulatory interactions are biologically meaningful. GRaNPA is a general framework for any GRN to evaluate their biological relevance by predicting cell-type specific differential expression.
Imports:
    futile.logger,
    caret,
    ranger,
    ggplot2,
    wesanderson
Depends:
    R (>= 3.6),
    tidyverse,
    tidyr,
    dplyr,
    magrittr,
    stats
License: LGPL (>= 3)
Encoding: UTF-8
LazyData: true
BugReports: https://git.embl.de/grp-zaugg/GRaNPA/issues
RoxygenNote: 7.1.2
Suggests: 
    testthat (>= 3.0.0)
Config/testthat/edition: 3
