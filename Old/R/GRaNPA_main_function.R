#'  Main GRaNPA function. it will use differential expression data to construct a random forest model to predict it using the information from GRN.
#'
#' @export
#' @import dplyr
#' @param DE_data Differential expression data. The DE matrix should contain 'ENSEMBL', 'logFC' and 'padj' columns
#' @param GRN_matrix_filtered A data.frame with at least 2 columns contain TF.name column for TF names and gene.ENSEMBL for gene ENSEMBL ids. Optionally it can have a weight columns for weighting the connections
#' @param DE_pvalue_th a cut off on adjusted pvalue for filtering the DE data. Default is 0.2
#' @param logFC_th a cut off on absolute log2 Fold Change for filtering the DE data. Default is 0
#' @param num_run Number of runs for real GRN (should be > 0). Default is 5 and at least 3 times is suggested.
#' @param num_run_CR Number of runs for quality control GRN (should be > 0). Default is 2 and at least 2 times is suggested.
#' @param num_run_random Number of runs for randomized GRN (should be > 0). Default is 5 and at least 3 times is suggested.
#' @param cores Number of cores. default is 10.
#' @param importance this is the algorithm to use for finding most important TFs. Default is permutation. impurity_corrected and impurity are the other options.
#' @param ML_type Could be regression or classification. For regression it computes R^2 to predict actual values. For classification it computes accuracy to predict directionality of DE data.
#' @param control Could be "cv" for " 10-fold cross validation" or 'oob' for "Out Of Bag" or 'bt' for "Bootstrap"
#' @param train_part You can divide genes into the train and test. Here you can mention how much of data you want to use for training
#'
#' @return a GRaNPA object contains
#'    normal_dist = distribution of R^2 for actual network,
#'    random_dist = distribution of R^2 for random network,
#'    CR_dist = distribution of R^2 for QC network,
#'    nrm_imp_unscaled = unscaled importance score for each TF and each run
#'    nrm_imp_scaled = scaled importance score for each TF and each run
#'    normal_data = The actual data matrix which the RF has been applied
#'    normal_models = all the RF models for actual network
#'    random_models = all the RF models for random network
#' @examples
#' \dontrun{
#' GRaNPA_main_function(DE_data, GRN_matrix_filtered)
#' }
#' @export
#'

GRaNPA_main_function <- function(DE_data,
                                            GRN_matrix_filtered,
                                            DE_pvalue_th = 0.2,
                                            logFC_th = 0,
                                            num_run = 5,
                                            num_run_CR = 2,
                                            num_run_random = 5,
                                            cores = 10,
                                            importance = "permutation",
                                            ML_type = "regression",
                                            control = "cv",
                                            train_part = 1){

  ### CHECK
  futile.logger::flog.info("GRaNPA Main function")
#  futile.logger:: flog.warn("In this version, the GRN should be filtered before")
  futile.logger::flog.warn("Both Differentiall expression data and GRN should contain 'ENSEMBL ID' for the list of genes")
  futile.logger::flog.info("Differential expression will be filltered by %s adjusted pvalue and %s absolute logFC", DE_pvalue_th, logFC_th)

  start.all = Sys.time()
  ### load packages here

  ## GRN should contain TF.name, gene.ENSEMBL, their weighting method , select(c("TF.name","gene.ENSEMBL","weight")
  if ( sum(c("TF.name","gene.ENSEMBL") %in% colnames(GRN_matrix_filtered)) < 2 ){
    futile.logger::flog.error("The GRN should contain 'TF.name' column for TF names and 'gene.ENSEMBL' for gene ENSEMBL ids")
    stop("The GRN should contain 'TF.name' column for TF names and 'gene.ENSEMBL' for gene ENSEMBL ids")
  }

  if ( sum(c("ENSEMBL" , "logFC" , "padj" ) %in% colnames(DE_data)) < 3){
    futile.logger::flog.error("The DE matrix should contain 'ENSEMBL', 'logFC' and 'padj' columns")
    stop("The DE matrix should contain 'ENSEMBL', 'logFC' and 'padj' columns ")
  }
  DE_data = DE_data[DE_data$padj < DE_pvalue_th,]
  DE_data = DE_data %>% dplyr::filter( abs(logFC) > logFC_th)
  ## this potentially could change to a minimum number of DE genes
  if( nrow(DE_data) == 0 ) {
    futile.logger::flog.error("The DE matrix has 0 rows after filtering. Please check the DE data or change the thresholds")
    stop("The DE matrix has 0 rows after filtering. Please check the DE data or change the thresholds")
  }

  all_raw_data = list(DE_data, as.data.frame(GRN_matrix_filtered))

  l_dist = v4_GRN_complete_dist(all_raw_data = all_raw_data,
                                num_run = num_run , num_run_CR = num_run_CR, num_run_random = num_run_random,
                                cores = cores, importance = importance,
                                ML_type = ML_type, control = control, train_part = train_part)

  futile.logger::flog.info("GRaNPA analysis has been finished! Preparing output object...")

  final_result = list()
  final_result$GRN = GRN_matrix_filtered
  final_result$normal_dist = l_dist$normal_dist
  final_result$random_dist =  l_dist$random_dist
  final_result$CR_dist =  l_dist$CR_dist
  final_result$nrm_imp_unscaled = l_dist$nrm_imp_unscaled
  final_result$nrm_imp_scaled = l_dist$nrm_imp_scaled
  final_result$normal_data = l_dist$normal_data
  final_result$normal_models = l_dist$normal_models
  final_result$random_models = l_dist$random_models


  .printExecutionTime(start.all)

  return(final_result)
}
