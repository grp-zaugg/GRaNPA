

### need to in stall bslib

# install.packages("bslib") # DONE
# BiocManager::install("BiocStyle")

pkgdown::build_site()

pkgdown::build_articles()

pkgdown::build_news()

pkgdown::build_reference(examples = F)

#### adding data into the package


library(dplyr)
GRaNPA_eGRN_example = read.delim("~/Downloads/Macrophages_control_naive_connections_expression.filtered.final.tsv" , header = T)
GRaNPA_Dorothea_A = read.table(gzfile("~/Downloads/dorothea_network_human_v2.tsv"), fill = TRUE, header = TRUE)
GRaNPA_Dorothea_A = GRaNPA_Dorothea_A %>% filter(confidence %in% c("A"))
GRaNPA_Dorothea_A$mor = NULL

DE = read.table(gzfile("~/Downloads/results_DESeq_condition_SL1344_vs_naive.tsv.gz"), fill = TRUE, header = TRUE)
DE$logFC  = DE$log2FoldChange.shrunk
DE$log2FoldChange.shrunk = NULL
DE$log2FoldChange.shrunk.abs = NULL
GRaNPA_DE_example = DE
usethis::use_data(GRaNPA_Dorothea_A,GRaNPA_eGRN_example,GRaNPA_DE_example,overwrite = TRUE)
