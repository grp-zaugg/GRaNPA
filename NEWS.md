<!--
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-->


# GRaNPA 0.1.15 (2021-12-14)

* This is the first release of GRaNPA.


# GRaNPA 0.1.2 (2022-09-06)

* Performance improvement and input and output naming.


# GRaNPA 1.0.0 (2024-05-05)

## Major Updates

- **Improved Efficiency**: GRaNPA new version introduces significant improvements in efficiency over the previous version. This update optimizes internal algorithms to handle data more effectively, ensuring faster processing times.

- **Pre-filtered Inputs**: In this version, both the network and gene matrix inputs need to be pre-filtered and prepared by the user. This change allows for greater flexibility and customization in data analysis, aligning input data more closely with specific research needs.

- **Single Run Machine Learning Model**: The machine learning model now runs only once per analysis, unlike multiple runs in previous versions. Recent publications have demonstrated that the results are robust across multiple runs, allowing us to streamline the process without compromising the quality of the analysis.

## Notes
- Users are encouraged to ensure that their data is appropriately pre-filtered and structured according to the new guidelines provided in the documentation. This preparation is crucial for leveraging the improved efficiency and accuracy of GRaNPA.

## Upgrade Recommendations
- It is recommended for all users to upgrade to this latest version to take full advantage of the new features and improvements. Please refer to the installation guide for detailed steps on upgrading.


# GRaNPA 1.0.2 (2024-08-23)
* Small bug fixed. 

