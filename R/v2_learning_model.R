pv_learning_model <- function(complete_matrix, num_core = 8, importance = "permutation", Random = FALSE, ml_method = "ranger"){

  flog.info("Start of applying learning model")

  # Determine the number of features available for modeling
  num_features = ncol(complete_matrix)

  # Check if there are enough features to proceed
  if(num_features > 2) {
    # Set up cross-validation training control
    train_control_cv <- caret::trainControl(method="cv", number=10, savePredictions = TRUE)

    flog.info("number of features: %s", num_features)
    flog.info("number of Genes: %s", nrow(complete_matrix))

    # Train the model using the specified machine learning method
    res.rf = caret::train(y ~ ., data = complete_matrix, method = ml_method,
                          trControl = train_control_cv, num.threads = num_core,
                          importance = importance)

    # Prepare the results data frame
    df_res = data.frame(rsq = mean(res.rf$resample$Rsquared), ngenes = nrow(complete_matrix))
  } else {
    flog.info("The number of features is less than required; at least two features are needed.")
    stop("The number of features is less than required; at least two features are needed.")
  }

  # Mark results as random if applicable
  df_res$random = Random

  return(list(report = df_res, model = res.rf))
}
