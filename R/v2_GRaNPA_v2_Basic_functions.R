#' GRaNPA Analysis Function
#'
#' This function uses gene-related data alongside a gene regulatory network matrix (GRN_matrix) to construct machine learning models, primarily using the Random Forest method via the 'ranger' package. It focuses on predicting gene-related outcomes and identifying key transcription factors.
#' @export
#' @param gene_metric_data A data frame containing gene-related predictive data.
#'        Expected to include the following columns:
#'        \itemize{
#'          \item \code{gene_name}: This column contains the names of genes, which could be ENSEMBL IDs or gene symbols. These names must match with the \code{gene_name} column in the \code{GRN_matrix} dataset.
#'          \item \code{value}: This column contains any quantitative measure related to the genes, such as log2 fold change values for differential expression analysis. If your data represents differential expression, please consider filtering the data before providing it to the function.
#'        }
#' @param GRN_matrix A data frame containing features with three columns:
#'        \itemize{
#'          \item \code{gene_name}: This column contains the names of target genes, which could be ENSEMBL IDs or gene symbols. These names must match with the \code{gene_name} column in the \code{gene_metric_data} dataset.
#'          \item \code{feature}: This column contains the transcription factors (TFs) that regulate the genes.
#'          \item \code{weight}: This column contains the weights from the GRN methods. If your GRN does not have weights, you can use 1 for all of them.
#'        }
#' @param num.cores The number of processor cores to use for parallel processing. Default is 8.
#' @param ml_method The machine learning method to use, defaults to 'ranger'.
#' @param importance The method to assess feature importance, default is 'permutation'.
#'        Other options include 'impurity' and 'impurity_corrected'.
#' @param Random_control A logical indicating whether to include random control models to validate the
#'        predictive significance. Default is TRUE.
#'
#' @return A list containing the following components:
#'        \itemize{
#'          \item \code{result}: A data frame of analysis results including model performance metrics.
#'          \item \code{models}: A list of models generated, including those based on actual data and randomized controls.
#'        }
#'
#' @examples
#' \dontrun{
#' gene_metric_data <- data.frame(gene.name = c("gene1", "gene2"), value = c(2.3, -1.2))
#' GRN_matrix <- data.frame(gene_name = c("gene1", "gene2"), feature = c("TF1", "TF2"), weight = c(1, 1))
#' results <- GRaNPA_Analysis(gene_metric_data, GRN_matrix = GRN_matrix)
#' }
#' @export




GRaNPA_Analysis <- function(gene_metric_data,
                            GRN_matrix,
                            num.cores = 8,
                            ml_method = "ranger",
                            importance = "permutation",
                            Random_control = TRUE){

  #### requirements
  require(futile.logger)
  require(dplyr)
  require(tidyverse)
  require(caret)
  require(tibble)

  futile.logger::flog.info("GRaNPA version 2, predicting gene-specific values based on network data and reporting important transcription factors that can explain variations. Ensure input data is pre-filtered.")

  #### check variables
  pv_variable_check(gene_metric_data, GRN_matrix, num.cores, ml_method, importance, Random_control)
  # Note: Ensure that the `gene_metric_data` is already filtered as per your analysis requirements before passing to this function.


  #### preparing data
  complete_matrix = data_preparation(gene_metric_data, GRN_matrix)

  # Summing non-'y' columns to check for connections, assuming 'y' is your response variable in the data
  complete_matrix %>%
    dplyr::select(-y) %>%
    apply(1, sum) -> tmp

  # Filtering out genes that have no connections or for which there is no information
  gene_metric_data_filtered = gene_metric_data %>%
    dplyr::filter(gene_name %in% names(tmp[tmp != 0]))

  # Restrict the complete matrix to the filtered gene names
  complete_matrix = complete_matrix[gene_metric_data_filtered$gene_name,]



  # Initialize containers for storing the models and their results
  final_models = list()
  final_res = tibble(type = character(), rsq = numeric())

  # Run the learning model on the prepared and filtered data
  res = pv_learning_model(complete_matrix, num_core = num.cores, importance = importance, ml_method = ml_method)

  # Store the model and its performance metrics for the actual data
  final_models[["Real"]] = res$model
  final_res = res$report

  # If random control is enabled, permute the response variable and re-run the model to simulate null hypothesis
  if(Random_control == TRUE) {
    complete_matrix_rand = complete_matrix
    complete_matrix_rand$y = sample(complete_matrix_rand$y , replace = F)

    res = pv_learning_model(complete_matrix_rand, num_core = num.cores, importance = importance, Random = TRUE, ml_method = ml_method)
    # Store the model and its performance metrics for the randomized data
    final_models[[("Random")]] = res$model
    final_res = rbind(final_res, res$report)
  }

  # Compile all results and models into a single list for return
  results = list(result = final_res, models = final_models)

  return(results)
}









