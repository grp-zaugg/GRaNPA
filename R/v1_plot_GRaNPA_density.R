
#'  plot a density figure
#'
#' @export
#' @param GRaNPA.object GRaNPA object, An output from GRaNPA main function.
#' @param outputFolder Absolute file path. No default.
#' @param plot_name Exact name of the plot
#' @param width width of the plot which be used by ggsave
#' @param height height of the plot which be used by ggsave
#' @param range optional range of the R^2 (xaxis) for comparing different density plot with each other. Default is NULL.
#' @return The final density plot
#' @examples
#' \dontrun{
#' oldversion_plot_GRaNPA_density(GRaNPA.object, "your/path/" , "density_plot" , width = 8, height = 6)
#' }
#' @export
#'
oldversion_plot_GRaNPA_density <- function(
  GRaNPA.object, ## this should be a object from prediction power pipeline.
  outputFolder,
  plot_name,
  width = 5,
  height = 5,
  range = NULL
) {

  data = data.frame(
    r2 = c(GRaNPA.object$normal_dist$N_ranger_model_rsq, GRaNPA.object$random_dist$RG_ranger_model_rsq, GRaNPA.object$CR_dist$RC_ranger_model_rsq),
    type = rep(c("Actual GRN" , "Randomized GRN" , "QC GRN") , c(nrow(GRaNPA.object$normal_dist), nrow(GRaNPA.object$random_dist), nrow(GRaNPA.object$CR_dist)))
  )

  grn.labs <- c("Randomized GRN",  "QC GRN" , "Actual GRN")
  names(grn.labs) <- c("Randomized GRN", "QC GRN", "Actual GRN")

  data$type = factor(data$type , grn.labs)

  p_fig = data %>% ggplot2::ggplot(aes(r2, fill = type)) + ggplot2::geom_density( alpha = 0.7) +
    ggplot2::theme_bw() +
    ggplot2::scale_fill_manual( values = wesanderson::wes_palette("Darjeeling1", 3, type = "discrete") , labels = grn.labs , name = "GRN type") +
    ggplot2::xlab(expression(paste("R"^"2"))) + ggplot2::ylab("Density") + ggplot2::ggtitle("") + ggplot2::guides(fill = FALSE)

  if (!is.null(range)){
    p_fig = p_fig + ggplot2::xlim(range)
  }

  ggplot2::ggsave(plot = p_fig, filename = plot_name, device = "pdf", path = outputFolder, width = width, height = height)
  return(p_fig)
}

