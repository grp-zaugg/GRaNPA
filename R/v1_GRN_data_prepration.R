

v4_GRN_data_prepration <- function( all_raw_data,
                                    TF_fdr_th = 1,
                                    directionality.only)  {

  ## DE here should be filtered
  futile.logger::flog.info("Preparing the Gene-TF Matrix.")
  st.time = Sys.time()

  if( length(all_raw_data) != 2 ) {
    futile.logger::flog.error("You need to provide 2 matrix for v4_GRN_data_prepration")
    stop("You need to provide 2 matrix for v4_GRN_data_prepration")
  }

  DE_data = all_raw_data[[1]]
  GRN = all_raw_data[[2]]

  GRN$weight = 1


  GRN  = GRN %>% ungroup() %>% dplyr::select(c("TF.name","gene.ENSEMBL","weight"))
  GRN = as.data.frame(GRN)

  ### create matrix for data I am adding correlation as weight
  data = matrix(0,nrow=length(unique(GRN$gene.ENSEMBL)) , ncol = length(unique(GRN$TF.name)))
  data = as.data.frame(data , row.names = unique(GRN$gene.ENSEMBL) )
  colnames(data) = unique(GRN$TF.name)

  for ( i in 1:nrow(GRN) ){
    rw = as.character(GRN$gene.ENSEMBL[i])
    cl = as.character(GRN$TF.name[i])
    weight = as.numeric(GRN$weight[i])

    data[rw,cl] <- weight
  }

  futile.logger::flog.info("The matrix has been created.")
  .printExecutionTime(st.time)

  cn.time = Sys.time()
  DE_data = DE_data %>% dplyr::select(c("ENSEMBL","logFC"))
  DE_data = DE_data[!duplicated(DE_data$ENSEMBL),] ## remove duplicates
  DE_data = DE_data[!is.na(DE_data$ENSEMBL),] ## remove NA
  DE_data$ENSEMBL = gsub(DE_data$ENSEMBL , pattern = "\\..*" , replacement = "") ## remove everything after "."

  rownames(DE_data) = DE_data$ENSEMBL

  gene_intersect = intersect(DE_data$ENSEMBL , rownames(data))
  DE_data = DE_data[gene_intersect,]
  data = data[as.character(DE_data$ENSEMBL),]
  data$y = rep(0,nrow(data))
  data[as.character(DE_data$ENSEMBL),]$y = as.numeric(DE_data$logFC)

  if ( directionality.only == TRUE) {
    data$y = ifelse(data$y > 0 , "positive", "negative")
  }
  futile.logger::flog.info("Final data prepration finished.")
  futile.logger::flog.info("The dimensions of final matrix are ( %s , %s )." , dim(data)[1] , dim(data)[2])
  .printExecutionTime(cn.time)
  return(data)
}
