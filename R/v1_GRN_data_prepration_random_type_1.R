v4_GRN_data_prepration_random_type_1 <- function(all_raw_data,directionality.only) {
  DE_random = all_raw_data[[1]]
  GRN_randomize = all_raw_data[[2]]
  DE_random$logFC = stats::runif(nrow(DE_random), -10 , 10)
  GRN_randomize$ENSEMBL = GRN_randomize$ENSEMBL[sample(1:nrow(GRN_randomize))]
  GRN_randomize$corr = stats::runif(nrow(GRN_randomize) , -1 , 1)
  data = v4_GRN_data_prepration(list(DE_random,GRN_randomize), directionality.only = directionality.only)
  return(data)
}
