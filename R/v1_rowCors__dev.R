rowCors__dev = function(x, y) {
  sqr = function(x) x*x

  if(!is.matrix(x)||!is.matrix(y)||any(dim(x)!=dim(y)))
    stop("Please supply two matrices of equal size. Correlation matrix")

  x   = sweep(x, 1, rowMeans(x))
  y   = sweep(y, 1, rowMeans(y))
  cor = rowSums(x*y) /  sqrt(rowSums(sqr(x))*rowSums(sqr(y)))
}

.printExecutionTime <- function(startTime, prefix = " ") {

  endTime  <-  Sys.time()
  futile.logger::flog.info(paste0(prefix, "Finished sucessfully. Execution time: ", round(endTime - startTime, 1), " ", units(endTime - startTime)))
}
